<?php

class Playlist extends Eloquent  {
	
	
	protected $table = 'playlists';
	protected $primaryKey = "playlistid";
	
	
    public function user()
    {
      return $this->belongsTo('User', 'usuarioid', 'usuarioid');
    }
  
  // public static function comentarios($id){
    // return DB::table('comentarios AS c')
    // ->select('c.text','c.second','u.username','c.usuarioid','u.avatar')
    // ->leftJoin('usuarios AS u','u.usuarioid','=','c.usuarioid')
    // ->where('c.cancionid',$id)
    // ->orderBy('c.created_at')
    // ->get();
  // }
	
}
