<?php

class Favorito extends Eloquent  {
	
	
	protected $table = 'favoritos';
	protected $primaryKey = "favoritoid";
	
	
    public function user()
    {
      return $this->belongsTo('User', 'usuarioid', 'usuarioid');
    }
  
  public static function getFavoritos($id){
    return DB::table('favoritos AS f')
			->select('f.favoritoid','u.username','c.usuarioid','c.cancionid','c.titulo')
			->leftJoin('canciones AS c','c.cancionid','=','f.cancionid')
			->leftJoin('usuarios AS u','u.usuarioid','=','c.usuarioid')
			->where('f.usuarioid',$id)
			->get();
  }
	
}
