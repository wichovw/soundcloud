<?php

class UsuarioController extends BaseController {

	public function perfil($id=null)
	{
		if($id==null || $id==Auth::id())	
			return View::make('perfil');
		else{
			$user = User::find($id);
			$following = Follow::whereRaw("sigue_a = $id AND usuarioid = ".Auth::id())->count()>0;

			return View::make('visita')->with('usuario',$user)->with('following',$following);
		}
			
	}
	
	public function login()
	{
		$data = Input::only(['usuario','password']);
		if(Auth::attempt(['username' => $data['usuario'], 'password' => $data['password']])){
		
			return Redirect::to('/perfil');
			
		}
		return array("message"=>"Usuario/password invalido");
	}
	
	public function logout()
	{
		Auth::logout();
		return Redirect::to('/');
	}
	
	public function store()
	{
		$data = Input::only(['username','email','password_confirmation','password']);
			$validator = Validator::make(
							$data,
							[
									'username' => 'required|min:5|unique:usuarios',
									'email' => 'required|email|min:5|unique:usuarios',
									'password' => 'required|min:5|confirmed',
									'password_confirmation'=> 'required|min:5'
							],
							[
								'email.required' => 'Email requerido',
								'email.unique' => 'Email ya existe',
								'email.email' => 'Ingrese email valido',
								'username.unique' => 'Nombre de usuario ya existe',
								'username.required' => 'Nombre de usuario requerido',
								'password.required' => 'Password requerido',
								'password.confirmed' => 'No coinciden los password',
								'password_confirmation.required' => 'Confirme su password',
								'min'=>'El minimo de caracteres es 5'
								
							]
					);

		if($validator->fails()){
				return $validator->messages()->toJson();
		}
		$data['password']=Hash::make($data['password']);
		$newUser = User::create($data);
		Auth::login($newUser);
		 return Redirect::to('/perfil');
	}


  
    public function songs($id)
	{
		$canciones = Cancion::where("usuarioid","=",$id)->get();
		$likes = array();
		$tags = array();
		foreach($canciones as $song){
			$likes[$song->cancionid] = Favorito::where("cancionid","=",$song->cancionid)->count();
			$tags[$song->cancionid] = CancionTag::getTags($song->cancionid);
		}
		$user = User::find($id);
		return View::make('songs')
			->with("canciones",$canciones)
			->with("likes",$likes)
			->with("usuario",$user)
			->with("tags",$tags);
	}
  
    public function favs($id)
	{
		$favs = Favorito::getFavoritos($id);
		$cont = array();
		$tags = array();
		foreach($favs as $f){
			$cont[$f->cancionid] = Favorito::where("cancionid","=",$f->cancionid)->count();
			$tags[$f->cancionid] = CancionTag::getTags($f->cancionid);
		}
		$user = User::find($id);
		return View::make('favs')
			->with("usuario",$user)
			->with("favs",$favs)
			->with("tags",$tags)
			->with("cont",$cont);
		
	}
  
 	public function following()
	{
		return View::make('following');
	}
	
 	public function uploadAvatar(){
		$file = Input::file('imagen');
		$nombre = $file->getClientOriginalName();
		
		$ext=substr($nombre,strrpos($nombre,".",-1));
		
		
		if($ext==".jpg"||$ext==".jpeg"||$ext==".png"||$ext==".gif"){
			$nombre = $file->getClientOriginalName();
			$nombre = date('Ymdhi').$nombre;
			$file->move('public/images/', $nombre);
			$img = Image::make('public/images/'.$nombre)->resize(110, 110);
			$img->save('public/images/'.$nombre);
			
			$usuario = User::find(Auth::id());
			$usuario->avatar=$nombre;
			$usuario->save();
			return Redirect::to('/perfil');
		}
		else
			return array("message"=>"No es una Imagen");
	}
	
	public function favorito($id){
		$fav = new Favorito;
		$fav->usuarioid = Auth::id();
		$fav->cancionid = $id;
		$fav->save();
		
	
	}	
	
	public function quitarFavorito($id){
		$fav = DB::table('favoritos')->whereRaw("usuarioid = ".Auth::id()." AND cancionid = ".$id)->delete();
	}
	
	public function follow($id){
		$follow = new Follow;
		if($id==Auth::id())
			return array("message"=>"Invalido");
		$existe = User::find($id);
		if(!$existe)
			return array("message"=>"No existe usuario");
			
		$follow->sigue_a = $id;
		$follow->usuarioid = Auth::id();
		$follow->save();
	
	}
	
	public function unfollow($id){
		$unfollow = DB::table('follows')->whereRaw("usuarioid = ".Auth::id()." AND sigue_a = ".$id)->delete();
	}
	
}
