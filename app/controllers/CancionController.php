<?php

class CancionController extends BaseController {

	public function play($id)
	{
		
		$cancion = Cancion::find($id);
		$comentarios = Comentario::comentarios($id);
		$tags = CancionTag::getTags($id);
		
		

		
		
				
			
			
		
		
			
		if(Auth::check()){
			$playlists = DB::table('playlists')
				->whereRaw("playlistid NOT IN (SELECT playlistid FROM playlistcancion WHERE cancionid=$id)")
				->where('usuarioid','=',Auth::id())
				->orderBy('created_at', 'asc')
				->lists('nombre','playlistid');
			$fav = Favorito::whereRaw("usuarioid = ".Auth::id()." AND cancionid = ".$id)->count()>0;
		}else{
			$playlists = null;
			$fav = false;
		}
			

		if($cancion->usuarioid==Auth::id())
			return View::make('cancion/owner')
				->with('cancion',$cancion)
				->with('comentarios',$comentarios)
				->with('tags',$tags)
				->with('playlists',$playlists)
				->with('fav',$fav);
		else
			return View::make('cancion/play')
				->with('cancion',$cancion)
				->with('usuario',$cancion->User)
				->with('comentarios',$comentarios)
				->with('tags',$tags)
				->with('fav',$fav)
				->with('playlists',$playlists);
	}
	
	
	
	public function store()
	{
		$data = Input::only(['cancion','titulo']);
			$validator = Validator::make(
							$data,
							[
									'titulo' => 'required'
							],
							[
								'titulo.required' => 'Titulo requerido'
							]
					);
		$file = Input::file('cancion');
		
		$nombre = $file->getClientOriginalName();
		
		if(substr($nombre,strlen($nombre)-4)!=".mp3"){
			return array("message"=>"No es un archivo mp3");
		}
		if($validator->fails()){
				return $validator->messages()->toJson();
		}
		
		$nombre = date('Ymdhi').$nombre;
		$nombre = str_replace(" ","_",$nombre);
		$file->move('public/audio', $nombre);
		$time = Sonus::getMediaInfo('public/audio/'.$nombre)['streams'][0]['duration'];
		$pos = strrpos($time,".",-1);
		$cancion = new Cancion;
		$cancion->duracion = substr($time,0,$pos);
		$cancion->titulo = $data['titulo'];
		$cancion->filepath = $nombre;
		$cancion->usuarioid = Auth::id();
		$cancion->save();
		return Redirect::to('/cancion/play/'.$cancion->cancionid);
	}
	
	public function guardarTitulo($id)
	{
		$cancion = Cancion::find($id);
		$data = Input::only(['titulo']);
			$validator = Validator::make(
							$data,
							[
									'titulo' => 'required'
							],
							[
								'titulo.required' => 'Titulo requerido'
							]
					);
		if($validator->fails()){
				return $validator->messages()->toJson();
		}
		
		$cancion->titulo = $data['titulo'];
		$cancion->save();
		
		return array();
		
	}
 	public function uploadCover($id){
	
		$cancion = Cancion::find($id);
		if($cancion=="")
			App::abort(404);
		
		$file = Input::file('imagen');
		$nombre = $file->getClientOriginalName();
		
		$ext=substr($nombre,strrpos($nombre,".",-1));
		
		
		if($ext==".jpg"||$ext==".jpeg"||$ext==".png"||$ext==".gif"){
			$nombre = $file->getClientOriginalName();
			$nombre = date('Ymdhi').$nombre;
			$file->move('public/images/', $nombre);
			$img = Image::make('public/images/'.$nombre)->resize(110, 110);
			$img->save('public/images/'.$nombre);
			
			
			$cancion->cover=$nombre;
			$cancion->save();
			return Redirect::to('/cancion/play/'.$id);
		}
		else
			App::abort(404);
	}
	
 	public function search()
	{
		$data = Input::only(['busqueda']);
		$nombre = strtolower($data['busqueda']);
		$nombre = str_replace(" ",",",$nombre);
		$nombre = addslashes($nombre);
		
		$results = CancionTag::getCanciones($nombre);
		$info = array();
		foreach($results as $r){
			$info[$r->cancionid]=array();
		}
		
		foreach($info as $k=>$v){
			$cancion = Cancion::find($k);
			$fav = Favorito::where("cancionid","=",$k)->count();
			$tags = CancionTag::getTags($k);
			$user = Cancion::find($k)->user;
			$v['cancion'] = $cancion;
			$v['fav'] = $fav;
			$v['tags'] = $tags;
			$v['user'] = $user;
			$info[$k] = $v;
		}
		// print_r($info);
		// exit;
		return View::make('busqueda')
			->with("busqueda",$data['busqueda'])
			->with("datos",$info);
	}
}
