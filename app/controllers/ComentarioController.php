<?php

class ComentarioController extends BaseController {
  
  public function store()
  {
    $data = Input::only(['comentario','minuto','id']);
			$validator = Validator::make(
							$data,
							[
									'comentario' => 'required',
									'minuto' => 'required',
									'id' => 'required'
							],
							[
								'required' => 'Campos requeridos'
							]
					);
		
		
		
      if($validator->fails()){
              return $validator->messages()->toJson();
      }
    
    $cancion = Cancion::find(Crypt::decrypt($data['id']));
    if(!$cancion)
        return array("Cancion invalida");
    
    $minuto = "00:".$data['minuto'];
    $minuto = $this->convert($minuto);
    $dur = $this->convert($cancion->duracion);
    if($minuto>=$dur)
      return array(" El tiempo del comentario excede la duracion de la cancion");
    
    $comentario = new Comentario;
    $comentario->cancionid = Crypt::decrypt($data['id']);
    $comentario->usuarioid = Auth::id();
    $comentario->text = $data['comentario'];
    $comentario->second = $data['minuto'];
    $comentario->save();
    return array();
    

    
    
		
  }
  
  private function convert($time){
    $arr = explode(":",$time);
    $total = (int)$arr[0]*3600+(int)$arr[1]*60+(int)$arr[2];
    return $total;
  }
  
}
