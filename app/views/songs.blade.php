@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
    
  <div class="header-picture">
       @if($usuario->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{$usuario->avatar}}" id="avatar" alt="Profile Picture">
      @endif
  </div>
  <div class="header-info">
	<h3>{{$usuario->username}}</h3>
	<h1>{{"@".$usuario->username}}</h1>
  </div>
  <div class="usr-menu">
    <ul class="nav">
      <li><a href="/playlists/{{$usuario->usuarioid}}">Playlists</a></li>
      <li><a class="active" href="#">Canciones</a></li>
      <li><a href="/favoritos/{{$usuario->usuarioid}}">Favoritos</a></li>
      <li><a href="#">Siguiendo</a></li>
    </ul>
  </div>
</div>

<div class="feed songs">
	@foreach($canciones as $cancion)
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>{{$likes[$cancion->cancionid]}}</span>
      </div>
      <div class="delete-btn">
        <button>
          <i class="fa fa-trash"></i>
          Borrar
        </button>
      </div>
    </div>
    <h4>{{$cancion->titulo}}</h4>
    <button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>{{$usuario->username}}</p>
      <p>
				@foreach($tags[$cancion->cancionid] as $tag)
					<a href="/search?busqueda={{$tag->nombre}}" class="tag">{{$tag->nombre}}</a>
				@endforeach
			</p>
	</div>
  </div>
	@endforeach
	
	
</div>

@stop