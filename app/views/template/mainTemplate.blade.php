<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta NAME="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
	
	<title>Music player</title>
	{{ HTML::style('css/new/main.css'); }}
	@yield('stylesheets')

</head>
<body>
    <header class="main">
        <div class="container">
						<a class="home-link" href="/">
										 <button class="home-btn">
							 <i class="fa fa-soundcloud fa-2x"></i>
						 </button>
						</a>
            <ul class="buscar-form">
						{{ Form::open(array('route' => array('search'), 'id'=>'formBusqueda','method' => 'get')) }}
           <li>

           </li>
                <li class="search-box">
                    {{ Form::text('busqueda', null,array('placeholder'=>'Buscar...')) }}<!--
                    --><button type="submit" class="search-btn">
                        <i class="fa fa-search"></i>
                    </button>
                </li>
							{{ Form::close() }}
            </ul>
            <ul class="login-form">
						@if(!Auth::check())
                {{ Form::open(array('route' => array('login'), 'method' => 'post')) }}
                    <li>{{ HTML::link('#',"Registro",array("id"=>"link-registro")) }}</li>
                    <li>{{ Form::text('usuario', null,array('class' => '','placeholder'=>'Usuario')) }}</li>
                    <li>{{ Form::password('password', null,array('class' => '','placeholder'=>'Contraseña')) }}</li>
                    <li>{{ Form::submit('Ingresar', array('class' => 'login-btn')) }}</li>
						@else
								{{ Form::open(array('route' => array('logout'), 'method' => 'post')) }}
										<li>{{ HTML::link('#',"Subir cancion",array("id"=>"link-upload")) }}</li>
										<li>{{ HTML::link('perfil',"Mi perfil") }}</li>
										<li>{{ Form::submit('Cerrar sesion', array('class' => 'login-btn')) }}</li>

						@endif
						{{Form::close()}}
            </ul>
        </div>
    </header>


    <div class="container">
        <section class="main">
           
            @yield('content')
            
        </section>
    </div>
    @if(!Auth::check())
    <div class="modal registro-modal" hidden>
        <button class="close-btn">
            <i class="fa fa-times"></i>
        </button>
        <h2 class="modal-title">Registro</h2>
        <div class="container">
           
            {{ Form::open(array('route' => array('usuario.store'),'id'=>'registerForm', 'method' => 'post', 'class' => 'dl')) }}

              <dl>
                <dt>Nombre de Usuario</dt>
                <dd>
                   {{Form::text('username', null,array('placeholder' => 'Ingrese Usuario'))}}
                </dd>
                <dt>Email</dt>
                <dd>
                 {{Form::text('email', null,array('placeholder' => 'Ingrese Email'))}}
                </dd>
                <dt>Password</dt>
                <dd>
                 {{Form::password('password',array('placeholder' => 'Ingrese Password'))}}
                </dd>
                <dt>Re-escriba password</dt>
                <dd>
									{{Form::password('password_confirmation',array('placeholder' => 'Re-escriba password'))}}
                 
                </dd>
                <dt></dt>
                <dd>
                  {{Form::submit('Registrarse', array('class' => 'register-btn','id'=>'register'))}}
                </dd>
              </dl>
           {{ Form::close() }}
        </div>
    </div>
		@else
    <div class="modal upload-modal" hidden>
        <button class="close-btn">
            <i class="fa fa-times"></i>
        </button>
            <h2 class="modal-title">Subir cancion</h2>
        <div class="container">
            {{ Form::open(array('route' => array('cancion.store'), 'method' => 'post','class' => 'dl','files' => true)) }}
              <dl>
                <dt>Titulo</dt>
                <dd>
                   {{Form::text('titulo', null,array('placeholder' => 'Ingrese Titulo'))}}
                </dd>
                <dt>Archivo (solo mp3)</dt>
                <dd>
                 {{ Form::file('cancion', ['class' => 'subirCancion']) }}
                </dd>
                <dt></dt>
                <dd>
                  {{Form::submit('Subir', array('class' => 'register-btn'))}}
                </dd>
              </dl>
           {{ Form::close() }}
        </div>
    </div>
		@endif
   
    <div class="fondo-modal" hidden></div>


	{{ HTML::script('js/jquery-1.11.0.min.js'); }}
	<script type="text/javascript">
      
      jQuery("#link-registro").click(function(){
        jQuery(".fondo-modal").fadeIn();
        jQuery(".registro-modal").slideDown();
      });      
			
			jQuery("#link-upload").click(function(){
        jQuery(".fondo-modal").fadeIn();
        jQuery(".upload-modal").slideDown();
      });

      jQuery(".modal button.close-btn, .fondo-modal").click(function(){
        jQuery(".fondo-modal").fadeOut();
        jQuery(".modal").slideUp();
      });
        
			$("#register").click(function(e){
				e.preventDefault();
				$.post("/usuario",$("#registerForm").serialize(),function(data){
          var message = "";
          $.each(data, function(i, val) {
            message+=val+"\n";
          });
          if(message.length>0)
            alert(message);
					else{
						windows.location.href = "/perfil"
					}
				},'json');
			});

					
			
			
    </script>
	@yield('scripts')

</body>
</html>