@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
     @if($following)
      <button id="unfollow" class="follow"><i class="fa minus-circle"></i> Abandonar</button>
      <button id="follow" class="follow hide"><i class="fa fa-rocket"></i> Seguir</button>
    @else
      <button id="unfollow" class="follow hide"><i class="fa minus-circle"></i> Abandonar</button>
      <button id="follow" class="follow"><i class="fa fa-rocket"></i> Seguir</button>
    @endif
	<div class="header-picture">
      @if($usuario->avatar=="")
        <img src="/images/cover.jpg" id="avatar" alt="Profile Picture">
      @else
        <img src="/images/{{$usuario->avatar}}" id="avatar" alt="Profile Picture">
      @endif
	</div>
	<div class="header-info">
	<h3>{{$usuario->username}}</h3>
	<h1>{{"@".$usuario->username}}</h1>
	</div>
	<div class="usr-menu">
	<ul class="nav">
	  <li><a href="#">Playlists</a></li>
	  <li><a href="#">Canciones</a></li>
	  <li><a href="#">Favoritos</a></li>
	  <li><a href="#">Siguiendo</a></li>
	</ul>
	</div>
</div>

<div class="feed">
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>31</span>
      </div>
    </div>
    <h4>Nombre de la canción</h4>
    <button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>@username ha agregado una nueva canción</p>
      <p><a href="#" class="tag">tag1</a><a href="#" class="tag">tag2</a><a href="#" class="tag">tag3</a></p>
    </div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href="" class="liked"><i class="fa fa-smile-o"></i></a>
        <span>12</span>
      </div>
    </div>
    <h4>Nombre de la playlist</h4>
    <button class="play"><i class="fa fa-play-circle-o"></i></button>
    <div class="post-info">
      <p>@username ha agregado una nueva playlist</p>
      <p><a href="#" class="tag">sólo este tag</a></p>
    </div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>2</span>
      </div>
    </div>
    <h4>@other_user</h4>
    <div class="post-info">
      <p>@username ahora sigue a @othe_user</p>
    </div>
  </div>
</div>

@stop


@section('scripts')

<script type="text/javascript">
$(document).ready(function(){
  $("#follow").click(function(){
    $.post("/follow/{{$usuario->usuarioid}}",function(data){
      $("#follow").toggleClass("hide");
      $("#unfollow").toggleClass("hide");
    });
  });
  $("#unfollow").click(function(){
    $.post("/unfollow/{{$usuario->usuarioid}}",function(data){
      $("#follow").toggleClass("hide");
      $("#unfollow").toggleClass("hide");
    });
  });
  
});
</script>
@stop