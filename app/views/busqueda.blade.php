@extends('template/mainTemplate')

@section('content')

<div class="playlist-desc page-header">
	<div class="header-picture">
		<i class="fa fa-3x fa-search"></i>
	</div>
	<div class="header-info">
		<h2>Resultados de búsqueda</h2>
   		<p>{{$busqueda}}</p>
    </div>
</div>

<div class="feed songs">
	@foreach($datos as $k=>$v)
  <div class="post">
    <div class="pull-right">
      <div class="likes">
        <a href=""><i class="fa fa-smile-o"></i></a>
        <span>{{$v['fav']}}</span>
      </div>

    </div>
    <h4>{{$v['cancion']->titulo}}</h4>
    <a href="/cancion/play/{{$v['cancion']->cancionid}}"><button class="play"><i class="fa fa-play"></i></button>
    <div class="post-info">
      <p>{{$v['user']->username}}</p>
      <p>
				@foreach($v['tags'] as $tag)
					<a href="/search?busqueda={{$tag->nombre}}" class="tag">{{$tag->nombre}}</a>
				@endforeach
			</p>
	</div>
  </div>
	@endforeach
	
	
</div>

@stop