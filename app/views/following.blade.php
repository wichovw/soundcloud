@extends('template/mainTemplate')

@section('content')

<div class="profile-desc page-header">
    <button class="follow"><i class="fa fa-rocket"></i> Seguir</button>
  <div class="header-picture">
    <img src="/images/cover.jpg" alt="Profile Picture">
  </div>
  <div class="header-info">
    <h3>User Name</h3>
    <h1>@username</h1>
  </div>
  <div class="usr-menu">
    <ul class="nav">
      <li><a href="#">Playlists</a></li>
      <li><a href="#">Canciones</a></li>
      <li><a href="#">Favoritos</a></li>
      <li><a class="active" href="#">Siguiendo</a></li>
    </ul>
  </div>
</div>

<div class="feed users">
  <div class="post">
    <div class="pull-right">
      <div class="delete-btn">
        <button>
          <i class="fa fa-minus-circle"></i>
          Abandonar
        </button>
      </div>
      <div class="followers-count">
        <span class="number">42</span>
        <span>seguidores</span>
      </div>
    </div>
    <button class="play"><i class="fa fa-user"></i></button>
    <div class="post-info">
      <h4>@otheruser</h4>
      <p>Nombre del usuario</p>
    </div>
  </div>
  <div class="post">
    <div class="pull-right">
      <div class="delete-btn">
        <button>
          <i class="fa fa-minus-circle"></i>
          Abandonar
        </button>
      </div>
      <div class="followers-count">
        <span class="number">1</span>
        <span>seguidor</span>
      </div>
    </div>
    <button class="play"><i class="fa fa-user"></i></button>
    <div class="post-info">
      <h4>@elmasbitchdetodos</h4>
      <p>Jorge Lainfiesta</p>
    </div>
  </div>
</div>

@stop